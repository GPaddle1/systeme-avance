Récupération et lancement du conteneur
```
docker pull jenkins/jenkins
docker run -p 8080:8080 --name=jenkins-master jenkins/jenkins
```

Pour vérifier la bonne marche du conteneur, se rendre avec un navigateur à http://localhost:8080

Arret du conteneur
```
docker stop jenkins-master
```