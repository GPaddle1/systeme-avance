# Cours de système avancé

## Exercices :

1. 🖊 `cp` : [Copy file](01-syscalls_cp)
2. 🖨 `reverse print` : [Print file by the end](01-syscalls_print_reverse)
3. 🖨 `ls` : [List files in directory](01-syscalls_ls)
--------------------
4. 🛠 `fork` : [Create a fork and describes it](02-syscalls_fork)
5. 🤿 `redirect_std_flow` : [Redirect a standard flow](02-syscalls_redirect-std-flow)
6. 🤿 `redirect_std_flow_pipe` : [Redirect a standard flow with a pipe](02-syscalls_redirect-std-flow-pipe)
--------------------
7. 🤿 `XXX_XXX` : [XXX XXX a standard flow with a pipe](02-syscalls_XXX_XXX) TODO

## Intervenant :

Pierre L. : <pierre1.leroy@orange.com>
