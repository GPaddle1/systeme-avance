/**
 * \file c_ls.c
 * \brief Basic parsing options c_ls.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \author Guillaume K. <keller.guillaume68@gmail.com>
 * \version 0.1
 * \date 10 septembre 2016
 * \date septembre 2021
 *
 * Basic parsing options c_ls exemple c file.
 */
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <grp.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096

#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS \
    "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name) {
    dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}

/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free) {
    if (to_free != NULL) free(to_free);
}

/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str() {
    char* str = NULL;

    if (optarg != NULL) {
        str = strndup(optarg, MAX_PATH_LENGTH);

        // Checking if ERRNO is set
        if (str == NULL)
            perror(strerror(errno));
    }

    return str;
}

/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {"input", required_argument, 0, 'i'},
        {0, 0, 0, 0}};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */

const char* binary_optstr = "hvi:o:";

void printError(char* text) {
    perror("Error ");
}

void describeError() {
    printError(strerror(errno));
}

void printFileName(char* name) {
    printf("%s", name);
}

void printRights(struct stat fileStat) {
    printf((S_ISDIR(fileStat.st_mode)) ? "d" : "-");
    printf((fileStat.st_mode & S_IRUSR) ? "r" : "-");
    printf((fileStat.st_mode & S_IWUSR) ? "w" : "-");
    printf((fileStat.st_mode & S_IXUSR) ? "x" : "-");
    printf((fileStat.st_mode & S_IRGRP) ? "r" : "-");
    printf((fileStat.st_mode & S_IWGRP) ? "w" : "-");
    printf((fileStat.st_mode & S_IXGRP) ? "x" : "-");
    printf((fileStat.st_mode & S_IROTH) ? "r" : "-");
    printf((fileStat.st_mode & S_IWOTH) ? "w" : "-");
    printf((fileStat.st_mode & S_IXOTH) ? "x" : "-");
}

void printOwner(uid_t uid) {
    struct passwd* user;
    user = getpwuid(uid);
    printf("%s", user->pw_name);
}

void printGroup(gid_t gid) {
    struct group* group;
    group = getgrgid(gid);
    printf("%s", group->gr_name);
}

void printSize(off_t size) {
    printf("%ld", size);
}

void printLastMaj(struct timespec time) {
    char timeStr[50];
    strftime(timeStr, 50, "%Y-%m-%d %H:%M:%S", localtime(&time));
    printf("%s", timeStr);
}

void printFileDetail(struct dirent* currentFile, char* bin_input_param) {
    printFileName(currentFile->d_name);
    printf("\t");

    struct stat fileStat;

    char filePath[80];
    strcpy(filePath, bin_input_param);
    strcat(filePath, "/");
    strcat(filePath, currentFile->d_name);

    if (stat(filePath, &fileStat) < 0) {
        describeError();
        exit(1);
    }

    printRights(fileStat);
    printf("\t");
    printOwner(fileStat.st_uid);
    printf(" : ");
    printGroup(fileStat.st_gid);
    printf("\t");
    printSize(fileStat.st_size);
    printf("\t");
    printLastMaj(fileStat.st_mtim);

    printf("\n");
}

/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv) {
    /**
   * Binary variables
   * (could be defined in a structure)
   */
    short int is_verbose_mode = 0;
    char* bin_input_param = NULL;

    // Parsing options
    int opt = -1;
    int opt_idx = -1;

    while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1) {
        switch (opt) {
            case 'i':
                //input param
                if (optarg) {
                    bin_input_param = dup_optarg_str();
                }
                break;
            case 'v':
                //verbose mode
                is_verbose_mode = 1;
                break;
            case 'h':
                print_usage(argv[0]);

                free_if_needed(bin_input_param);

                exit(EXIT_SUCCESS);
            default:
                break;
        }
    }

    /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
    if (bin_input_param == NULL) {
        dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

        // Freeing allocated data
        free_if_needed(bin_input_param);
        // Exiting with a failure ERROR CODE (== 1)
        exit(EXIT_FAILURE);
    }

    // Printing params
    dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %d\n",
            "input", bin_input_param,
            "verbose", is_verbose_mode);

    // Business logic must be implemented at this point

    //TODO

    DIR* dir = opendir(bin_input_param);

    if (dir == NULL) {
        describeError();
        return 1;
    }

    puts("Folder opened successfully");
    puts("--------------------------\n");

    struct dirent* currentFile = NULL;

    while ((currentFile = readdir(dir)) != NULL) {
        printFileDetail(currentFile, bin_input_param);
    }

    puts("\n--------------------------");
    if (closedir(dir) == -1) {
        puts("Problem during folder closing");
        return 3;
    } else {
        puts("Folder closed successfully");
    }

    // Freeing allocated data
    free_if_needed(bin_input_param);

    return EXIT_SUCCESS;
}
