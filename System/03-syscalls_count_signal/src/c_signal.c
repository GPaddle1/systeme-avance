/**
 * \file c_signal.c
 * \brief Basic parsing options c_signal.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \author Guillaume K. <keller.guillaume68@gmail.com>
 * \version 0.1
 * \date 10 septembre 2016
 * \date septembre 2021
 *
 * Basic parsing options c_signal exemple c file.
 */
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096

#define USAGE_SYNTAX "[OPTIONS]"
#define USAGE_PARAMS \
    "OPTIONS:\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name) {
    dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}

/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free) {
    if (to_free != NULL) free(to_free);
}

/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str() {
    char* str = NULL;

    if (optarg != NULL) {
        str = strndup(optarg, MAX_PATH_LENGTH);

        // Checking if ERRNO is set
        if (str == NULL)
            perror(strerror(errno));
    }

    return str;
}

/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {0, 0, 0, 0}};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */

const char* binary_optstr = "hvi:o:";

void redFG() {
    printf("\033[1;31m");
}
void yellowFG() {
    printf("\033[1;33m");
}
void greenFG() {
    printf("\033[0;32m");
}
void resetFG() {
    printf("\033[0m");
}

void printError(char* text) {
    printf("Error : %s\n", text);
}

void describeError() {
    redFG();
    printError(strerror(errno));
    resetFG();
}

int stop = 0;

void sig_term_hand(int sig) {
    char* separator = "---------";
    printf("Signal de fin reçu (%d)\n%s\n%s\n", sig, separator, separator);
    exit(0);
}

int compteur = 0;

void sig_int_hand(int sig) {
    printf("Compteur = ");
    greenFG();
    printf("%d\n", ++compteur);
    resetFG();
}

/**
 * Binary main loop
 *
 * \return 1 if it exit successfully
 */
int main(int argc, char** argv) {
    /**
     * Binary variables
     * (could be defined in a structure)
     */
    short int is_verbose_mode = 0;

    // Parsing options
    int opt = -1;
    int opt_idx = -1;

    while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1) {
        switch (opt) {
            case 'v':
                // verbose mode
                is_verbose_mode = 1;
                break;
            case 'h':
                print_usage(argv[0]);

                exit(EXIT_SUCCESS);
            default:
                break;
        }
    }

    // Printing params
    dprintf(1, "** PARAMS **\n%-8s: %d\n",
            "verbose", is_verbose_mode);

    // Business logic must be implemented at this point

    // SigTerm

    sigset_t sig_proc_sig_term;
    struct sigaction action_sig_term;

    sigemptyset(&sig_proc_sig_term);

    action_sig_term.sa_mask = sig_proc_sig_term;
    action_sig_term.sa_flags = 0;
    action_sig_term.sa_handler = sig_term_hand;

    sigaction(SIGTERM, &action_sig_term, 0);

    // SigInt

    sigset_t sig_proc_sig_int;
    struct sigaction action_sig_int;

    sigemptyset(&sig_proc_sig_int);

    action_sig_int.sa_mask = sig_proc_sig_int;
    action_sig_int.sa_flags = 0;
    action_sig_int.sa_handler = sig_int_hand;

    sigaction(SIGTERM, &action_sig_term, 0);
    sigaction(SIGINT, &action_sig_int, 0);

    // To launch a sigint signal from external source run :
    // kill -2 $(ps -e | grep c_signal | tail -1 | cut -d ' ' -f2)

    // To launch a sigterm signal from external source run :
    // kill -15 $(ps -e | grep c_signal | tail -1 | cut -d ' ' -f2)

    while (!stop) {
        sleep(1);
    }

    return EXIT_SUCCESS;
}
