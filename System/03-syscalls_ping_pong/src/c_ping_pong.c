/**
 * \file c_ping_pong.c
 * \brief Ping-pong implementation using signals : c_ping_pong.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \author Guillaume K. <keller.guillaume68@gmail.com>
 * \version 0.1
 * \date 10 septembre 2016
 * \date septembre 2021
 *
 * Basic parsing options c_ping_pong exemple c file.
 */
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096

#define USAGE_SYNTAX "[OPTIONS]"
#define USAGE_PARAMS \
    "OPTIONS:\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"

/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name) {
    dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}

/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free) {
    if (to_free != NULL) free(to_free);
}

/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str() {
    char* str = NULL;

    if (optarg != NULL) {
        str = strndup(optarg, MAX_PATH_LENGTH);

        // Checking if ERRNO is set
        if (str == NULL)
            perror(strerror(errno));
    }

    return str;
}

/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] =
    {
        {"help", no_argument, 0, 'h'},
        {"verbose", no_argument, 0, 'v'},
        {0, 0, 0, 0}};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */

const char* binary_optstr = "hvi:o:";

void resetFG() {
    printf("\033[0m");
}
void redFG() {
    printf("\033[1;31m");
}
void redFGWString(char* s) {
    redFG();
    printf("%s\n", s);
    resetFG();
}
void yellowFG(char* s) {
    printf("\033[1;33m");
    printf("%s\n", s);
    resetFG();
}
void blueFG(char* s) {
    printf("\033[1;34m");
    printf("%s\n", s);
    resetFG();
}
void greenFG(char* s) {
    printf("\033[1;32m");
    printf("%s\n", s);
    resetFG();
}

void printError(char* text) {
    printf("Error : %s\n", text);
}

void describeError() {
    redFG();
    printError(strerror(errno));
    resetFG();
}

int hitTheBall() {
    srand(time(NULL));
    return rand() % 500 < 400;
}

const int delay = 1;

void sig_lost_ball_handler(int);
void sig_parent_handler(int);
void sig_child_handler(int);

void child();
void parent(int);

/**
 * Binary main loop
 *%
 * \return 1 if it exit successfully
 */
int main(int argc, char** argv) {
    /**
     * Binary variables
     * (could be defined in a structure)
     */
    short int is_verbose_mode = 0;

    // Parsing options
    int opt = -1;
    int opt_idx = -1;

    while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1) {
        switch (opt) {
            case 'v':
                // verbose mode
                is_verbose_mode = 1;
                break;
            case 'h':
                print_usage(argv[0]);

                exit(EXIT_SUCCESS);
            default:
                break;
        }
    }

    // Printing params
    dprintf(1, "** PARAMS **\n%-8s: %d\n",
            "verbose", is_verbose_mode);

    // Business logic must be implemented at this point

    int pid = fork();

    if (pid == 0) {
        int childScore = 0;
        child();
    } else {
        int parentScore = 0;
        parent(pid);
    }

    return 0;
}

void parent(int childPid) {
    struct sigaction signal_action;
    sigset_t sig_proc_sig;
    signal_action.sa_handler = sig_parent_handler;
    sigemptyset(&signal_action.sa_mask);
    signal_action.sa_flags = 0;

    if (sigaction(SIGUSR1, &signal_action, 0) == -1) {
        perror("Pb with SIGUSR1 assignement");
        exit(1);
    }

    // -----------------------------------
    // -----------------------------------
    // -----------------------------------

    struct sigaction signal_action_ball;
    sigset_t sig_proc_sig_ball;
    signal_action_ball.sa_handler = sig_lost_ball_handler;
    sigemptyset(&signal_action_ball.sa_mask);
    signal_action_ball.sa_flags = 0;

    if (sigaction(SIGUSR2, &signal_action_ball, 0) == -1) {
        perror("Pb with SIGUSR2 assignement");
        exit(1);
    }

    // -----------------------------------
    // -----------------------------------
    // -----------------------------------

    while (1) {
        blueFG("Parent :");
        if (hitTheBall()) {
            sigfillset(&sig_proc_sig);
            sigdelset(&sig_proc_sig, SIGUSR1);
            sigdelset(&sig_proc_sig, SIGINT);
            sigsuspend(&sig_proc_sig);
            sleep(delay);
            kill(childPid, SIGUSR1);
        } else {
            blueFG("Loupe Parent");

            sigfillset(&sig_proc_sig_ball);
            sigdelset(&sig_proc_sig_ball, SIGUSR2);
            sigdelset(&sig_proc_sig_ball, SIGINT);
            sigsuspend(&sig_proc_sig_ball);

            kill(childPid, SIGUSR2);
        }

        blueFG("Ok Parent");
    }
}

void child() {
    struct sigaction signal_action;
    sigset_t sig_proc_sig;
    signal_action.sa_handler = sig_child_handler;
    sigemptyset(&signal_action.sa_mask);
    signal_action.sa_flags = 0;

    if (sigaction(SIGUSR1, &signal_action, 0) == -1) {
        perror("Pb with SIGUSR1 assignement");
        exit(1);
    }

    // -----------------------------------
    // -----------------------------------
    // -----------------------------------

    struct sigaction signal_action_ball;
    sigset_t sig_proc_sig_ball;
    signal_action_ball.sa_handler = sig_lost_ball_handler;
    sigemptyset(&signal_action_ball.sa_mask);
    signal_action_ball.sa_flags = 0;

    if (sigaction(SIGUSR2, &signal_action_ball, 0) == -1) {
        perror("Pb with SIGUSR2 assignement");
        exit(1);
    }

    // -----------------------------------
    // -----------------------------------
    // -----------------------------------

    while (1) {
        sleep(delay);

        greenFG("Child :");

        if (hitTheBall()) {
            kill(getppid(), SIGUSR1);

            sigfillset(&sig_proc_sig);
            sigdelset(&sig_proc_sig, SIGUSR1);
            sigdelset(&sig_proc_sig, SIGINT);
            sigsuspend(&sig_proc_sig);

        } else {
            greenFG("Child loupe");
            kill(getppid(), SIGUSR2);

            sigfillset(&sig_proc_sig_ball);
            sigdelset(&sig_proc_sig_ball, SIGUSR2);
            sigdelset(&sig_proc_sig_ball, SIGINT);
            sigsuspend(&sig_proc_sig_ball);
        }

        greenFG("Ok Child");
    }
}

void sig_lost_ball_handler(int signum) {
    redFGWString("Balle perdue !");
}

void sig_parent_handler(int signum) {
    blueFG("Ping Parent ...");
}

void sig_child_handler(int signum) {
    greenFG("Pong Child ...");
}

//     int gameNotFinish = 1;
//     // while (gameNotFinish) {
//     gameNotFinish = parentScore < 13 && childScore < 13;
//     greenFG("New game");

//     int pid = fork();

//     if (pid < 0) {
//         describeError();
//     } else if (pid == 0) {
//         printf("\tFils ppid %d\n", getppid()); //PID du père
//         printf("\tFils pid %d\n", getpid()); // PID du fils

//         kill(getppid(), SIGUSR1);
//     } else {
//         printf("\tPere %d\n", pid); // PID du fils
//         printf("\tPere pid %d\n", getpid()); // PID du père

//         kill(pid, SIGUSR1);
//     }

//     wait(NULL);
//     // }

//     return EXIT_SUCCESS;
// }
