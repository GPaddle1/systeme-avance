/**
 * \file c_print_reverse.c
 * \brief Basic parsing options c_print_reverse.
 * \author Pierre L. <pierre1.leroy@orange.com>
 * \author Guillaume K. <keller.guillaume68@gmail.com>
 * \version 0.1
 * \date 10 septembre 2016
 * \date septembre 2021
 *
 * Basic parsing options c_print_reverse exemple c file.
 */
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define STDOUT 1
#define STDERR 2

void redFG() {
    printf("\033[1;31m");
}
void yellowFG() {
    printf("\033[1;33m");
}
void greenFG() {
    printf("\033[0;32m");
}
void resetFG() {
    printf("\033[0m");
}

void printError(char* text) {
    redFG();
    printf("Error : %s\n", text);
    resetFG();
}

void describeError() {
    printError(strerror(errno));
}

int main(int argc, char** argv) {
    if (argc <= 1) {
        printError("1 argument is required");
        exit(EXIT_FAILURE);
    }

    printf("You began your sentence with : %s\n", argv[1]);
}
