# Retour sur expérience

## Points positifs

### Outil standard

L'utilisation de Jenkins est devenue un standard dans les nouveaux systèmes.

Le fait d'y avoir touché permet d'apercevoir les possibilités offertes par le système pour des projets personnels comme professionnels.

### As Code

La configuration as code est un domaine important dans un monde où l'intégration doit être la plus rapide et efficiente.

De ce fait, ne pas avoir à configurer ses services via les IHM mais via le code est un pas important vers l'automatisation des systèmes, cette initiation nous aura permis d'apercevoir ce qu'il est possible de mettre en place avec un système comme Jenkins.

## Difficultés

### Pipeline

La difficulté principale de ce projet aura été de trouver la manière de faire tourner la pipeline de création d'image correctement.
Nous nous sommes appuyés sur un [tutoriel](https://www.youtube.com/watch?v=HFQHB81vJHI) qui ne décrivait pas directement l'image utilisé pour jenkins, ce qui nous a fait perdre du temps dans le déploiement de la pipeline.
Celle-ci ne répondant pas positivement à cause de la non connaissance de docker.

Quelques recherches nous ont conduit vers une installation de docker au sein de l'image

Par la suite la syntaxe de Groovy a également nécessité une adaptation (notamment la syntaxe pour utiliser du code bash). Cependant l'utilisation de commande sh permet un plus grand confort dans l'initiation aux pipelines.

### DHCP

Le DHCP nous a également réservé des soucis, le lancement du conteneur dédié se terminait souvent brutalement (directement après le message d'initialisation).
L'utilisation d'une commande de fond a permis de maintenir le conteneur en vie après le lancement.

### Configuration Jenkins

Bien qu'avoir un conteneur Jenkins préconfiguré semble banal, les sujets sur le net traitant du sujet ne sont pas légion.

Il a donc été nécessaire de cherche longuement pour trouver comment préconfigurer l'installation des plugins ainsi qu'initier la pipeline.
Au final, la copie d'un fichier de configuration aura permis la préconfiguration de l'environnement.

### Configuration Docker/Windows

Le partage de certaines variables d'environnements a également posé des soucis, pour une raison encore inconnue, le `PATH` a été modifié, les `;` ont été remplacés par des espaces, ce qui a ajouté un temps de débug conséquent.

Cependant le mix WSL2 et Docker se passe plutôt bien, les commandes y sont fluides et ne nécessitent pas plus de configuration que l'utilisation via un vrai système linux.