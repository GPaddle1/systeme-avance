#!/bin/bash

mkdir -p jenkins 
sudo chown -R 1000 jenkins

# sudo groupadd docker || echo "Docker already exists"
# sudo usermod -aG docker $USER || echo "Usermod already done"
# newgrp docker || echo "New group already done"

sudo chmod 777 /var/run/docker.sock

echo "Building the image ..."

docker build . -t custom-jenkins || exit


echo "Launching the container ..."

sudo docker-compose up -d --remove-orphans || exit

cat << EOF
To connect for the first time run the next command :
	
		docker exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword

then
EOF