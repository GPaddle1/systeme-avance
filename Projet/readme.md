# DHCP as a Service
## Principe du système

La configuration réseau est gérée par un DHCP, celui-ci permet l'attribution des adresses IP au sein d'un réseau.

Le changement de configuration est souvent difficile à effectuer si l'on ne souhaite pas d'interruption de service. De ce fait, l'utilisation d'un conteneur Docker permet de minimiser le temps de coupure entre 2 configurations (arrêt du premier conteneur au dernier moment).

L'utilisation d'un système de versionnage permet également, en cas de problèmes de revenir rapidement à une configuration précédente dans le cas de soucis au sein de la configuration.
Enfin, cette implémentation a pour intérêt d'être automatisée (Scrutation du répo git par Jenkins et lancement de la nouvelle configuration automatiquement)

## Architecture

L'architecture de l'application est simple, elle consiste en une image jenkins en scrutation constante sur un répo git contenant la configuration DHCP.

On retrouve donc 2 répertoires majeurs :
- Jenkins (Cœur du projet)
- DHCP (Répertoire référence pour la configuration)

### Jenkins

Ainsi le répertoire contenant Jenkins est structuré par l'arborescence suivante :

```
.
├── DHCP_Pipeline
│   └── config.xml
├── docker-compose.yml
├── dockerfile
├── launch.sh
└── plugins.txt
```

| Fichier / Répertoire | Utilité |
|----------------------|---------|
| 📜 dockerfile | Se base sur une image Jenkins LTS puis installe l'ensemble des dépendances et plugins nécessaire à la bonne marche du futur conteneur |
| 📜 docker-compose.yml | Permet la liaison facile des volumes et des ports |
| 📜 DHCP_Pipeline | Contient la configuration de la pipeline qui sera lancée par la suite dans le conteneur Jenkins |
| 📁 jenkins | Répertoire mappée sur `/var/jenkins_home`, il contient l'ensemble des informations dont Jenkins a besoin pour fonctionner (binaires, plugins, workspaces, cache, ...) |
| 📜 plugins.txt | Plugins à installer par défaut dans le conteneur jenkins (git, pipelines, ...) |

### DHCP

Ce second répertoire comprend quant à lui la configuration DHCP à versionner, il est organisé selon l'arborescence suivante : 

```
.
├── Jenkinsfile
├── dhcpd
│   └── dhcpd.conf
├── docker-compose.yml
├── dockerfile
└── entrypoint.sh
```

| Fichier / Répertoire | Utilité |
|----------------------|---------|
| 📜 Jenkinsfile | Permet de décrire la pipeline utilisée pour traiter la configuration DHCP |
| 📜 dhcpd.conf | Fichier de configuration du service DHCP |
| 📜 dockerfile | Installation des différentes dépendances en rapport avec la configuration d'un service DHCP, ajout de la configuration DHCP à l'image |
| 📜 docker-compose.yml | Fichier de description du lancement de l'image décrite par le `dockerfile` |
| 📜 entrypoint.sh | Script de lancement de la configuration du DHCP et du réseau dédié |

## Configuration du projet

Par défaut le projet scrute toutes les minutes sur le répo `https://gitlab.com/GPaddle1/docker-dhcp.git`, ces deux informations peuvent être modifiées au sein du fichier `DHCP_Pipeline/config.xml`.

```xml
          <spec>* * * * *</spec>
          <ignorePostCommitHooks>false</ignorePostCommitHooks>
        </hudson.triggers.SCMTrigger>
      </triggers>
    </org.jenkinsci.plugins.workflow.job.properties.PipelineTriggersJobProperty>
  </properties>
  <definition class="org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition" plugin="workflow-cps@2640.v00e79c8113de">
    <scm class="hudson.plugins.git.GitSCM" plugin="git@4.10.0">
      <configVersion>2</configVersion>
      <userRemoteConfigs>
        <hudson.plugins.git.UserRemoteConfig>
          <url>https://gitlab.com/GPaddle1/docker-dhcp.git</url>
```

La ligne spec est au format CRON, ce qui permet de choisir la fréquence d'interrogation du repository git spécifié à la fin de l'extrait de code présent ci-dessus.

La durée entre deux scrutations pourra être ajustée en fonction de la fréquence de modification de la configuration DHCP ainsi que l'importance des modifications (En cas d'un changement d'urgence, la pipeline pourra être lancée manuellement).

Ces deux informations se retrouvent également dans la configuration du pipeline via l'IHM de Jenkins.

Le Jenkinsfile est à la disposition de l'émetteur du DHCP afin de lui permettre l'ajout aisé de tests ou d'autres modifications (lancement sur un autre environnement que celui qui contient Jenkins par exemple)

Le mappage des ports par défaut est le suivant : 

```yml
      - 8081:8080
      - 50000:50000
```

celui-ci pourra être modifié au besoin selon les utilisations en cours de la machine hôte.

## Lancement du projet

```sh
./launch.sh
```

Et voilà !

Jenkins sera par défaut lancé sur [http://localhost:8081](http://localhost:8081)

-------------

Le projet peut se lancer rapidement à l'aide du script `launch.sh`, celui-ci permet d'initialiser dans le répertoire l'intégralité des dépendances nécessaires, de build l'image, puis de la lancer.

Une fois démarré, le conteneur lance directement une pipeline avec la configuration DHCP en cours depuis le [repo de référence](https://gitlab.com/GPaddle1/docker-dhcp).

> ❗ Dans le cas d'un problème de droits à l'initialisation du conteneur, il peut être intéressant de lancer les commandes commentées au début du script `launch.sh`

```sh
sudo groupadd docker || echo "Docker already exists"
sudo usermod -aG docker $USER || echo "Usermod already done"
newgrp docker || echo "New group already done"
```

## Points traités

### FM

- [x] FM01 - La configuration du service répondant au schéma réseau est définie au sein d’un fichier dhcpd.conf
- [x] FM02 - La configuration DHCP doit être vérifiée avant de déclencher tout traitement.
- [x] FM03 - Le déclenchement du workflow d’intégration doit être associé à une action de commit au sein du dépôt.
- [x] FM04 - Le service DHCP est assuré par un conteneur docker. + Piloté via des jobs jenkins.
- [x] FM05 - Un test métier doit être réalisé à l’issue de toute mise à jour du service

### CT

- [x] CT01 - Le gestionnaire de sources à utiliser devra être git.
- [x] CT02 - L’intégration des différentes tâches doit être réalisée sous la solution jenkins.
- [x] CT03 - La définition du service Docker portant l’instanciation du conteneur DHCP doit être décrite sous un fichier docker-compose.yml.
- [x] CT04 - La définition des services de support git et jenkins doit-être décrite sous un fichier second fichier docker-compose.yml distinct.
- [x] CT05 - La persistance des différents des données (logs, fichiers de configuration, sources) sera assurée via des dockers volumes.
- [x] CT06 - Les fichiers produits doivent être documentés.
- [x] CT07 - La gestion des erreurs doit être prise en compte à tout niveau: vérification de configuration, création d’image

### CTO

- [x] CTO01 - La rédaction du Dockerfile passe une étape préalable de «lint»

## Retour sur expérience

Une restitution de ce qui a été fait peut être retrouvé [à cette adresse](./retours.md)

## Crédits

Guillaume Keller & Théo Welsch @ CNAM FIP2A Décembre 2021
